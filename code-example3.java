public void unregisterAndDeleteUser(User user){
        unregisterUser(user);
        deleteUser(user);
}

public void unregisterUser(User user){
        userDao.unregisterUser(user);
}

public void deleteUser(User user){
        userDao.deleteUser(user);
}
