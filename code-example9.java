//double indentation
if (booleanA || booleanB || 
        booleanC || booleanD || 
        booleanE) {
    System.out.println("***");
}


if (booleanA) System.out.println("***");  // NO!

if (booleanA) 
   System.out.println("***");  // YES!