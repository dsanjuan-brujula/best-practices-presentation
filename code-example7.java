package examples.java
public class Bicycle {
    public int cadence;
    public int gear;
    public int speed;
    public Bicycle(int startCadence, int startSpeed, int startGear) {
        gear = startGear;
        cadence = startCadence;
        speed = startSpeed;
    }
    public void setCadence(int newValue) {
        cadence = newValue;
    }
    public void setGear(int newValue) {
        gear = newValue;
    }
    public void applyBrake(int decrement) {
        speed -= decrement;
    }
    public void speedUp(int increment) {
        speed += increment;
    }
        
}



public class RigthBicycle {
        
    public int cadence;
    public int gear;
    public int speed;
        
    public RigthBicycle(int startCadence, int startSpeed, int startGear) {
        setGear( startGear );
        setCadence(startCadence );
        setSpeed() startSpeed );
    }
        
    public void setCadence(int newValue) {
        this.cadence = newValue;
    }
        
    public void setGear(int newValue) {
        this.gear = newValue;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }
        
    public void applyBrake(int decrement) {
        this.speed -= decrement;
    }
        
    public void speedUp(int increment) {
        this.speed += increment;
    }
        
}